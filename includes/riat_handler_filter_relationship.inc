<?php

class riat_handler_filter_relationship extends views_handler_filter_in_operator {
  function get_value_options() {
    $this->value_title = t('Node relationship type');
    $relationships = riat_get_all('node');
    foreach ($relationships as $key => $relationship) {
      $options[$key] = $relationship['title'];
    }
    $this->value_options = $options;
  }
}
